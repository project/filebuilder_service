<h2>Manage Authentication Key</h2>
This key is used to encrypt authentication data between the desktop client and this service. 
<br />
<br />
<?php
  if ($key !== NULL) {
    echo $export_key_link . ' | ';
  }
  else {
    echo "No key set yet (required to allow client communication):<br />";
  }
  
  echo $generate_key_link;
?>
