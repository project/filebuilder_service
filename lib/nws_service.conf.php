<?php

/**
 * @file
 * nws_services.conf.php
 * Filedepot: File Management Service Module developed by Nextide www.nextide.ca
 * Configuration page for version info
 */
class NWSServiceConfigurationValues
{

  /**
   * Returns the SFTP path filedepot builder requires to change directory to on the server
   */
  public static function getSFTPUploadReturnPath() {
    $path = file_directory_path() . "/filebuilder_working_directory/";
    if (!file_exists($path)) {
      $umask = umask(0);
      mkdir($path, 0777);
      umask($umask);
    }

    /*     * ******
      UNCOMMENT line 28 if your SFTP user account root directory does not point to "private://filebuilder_working_directory/"
     * ******* */
    //return NWSServiceConfigurationValues::getPathToFDP() . $path;
    return "";
  }

  public static function getPathToFDP() {
    return str_replace('\\', '/', getcwd());
  }

}

?>
